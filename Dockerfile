# dockerfile to deploy our projects.
FROM alpine:latest
MAINTAINER Dennis Timmermann <d@tmrmn.com>
RUN apk update

# we need rsync to deploy, we may fall back to ftp for some cases.
RUN apk add rsync lftp openssh-client sshpass
